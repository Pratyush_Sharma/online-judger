class AddDateOfBirthToUser < ActiveRecord::Migration
  def change
    add_column :users, :dateOfBirth, :date
  end
end
