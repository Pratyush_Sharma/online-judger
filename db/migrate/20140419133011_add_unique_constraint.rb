class AddUniqueConstraint < ActiveRecord::Migration
  def change
  	add_index :contests, [:contestName, :startDateTime], unique: true
  end
end
