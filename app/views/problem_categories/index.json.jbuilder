json.array!(@problem_categories) do |problem_category|
  json.extract! problem_category, :id, :problem_id, :category_id
  json.url problem_category_url(problem_category, format: :json)
end
